package uk.co.diggidy.localstackdemo.s3bucket;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.amazonaws.services.lambda.runtime.events.S3Event;

import java.util.function.Function;

@Slf4j
@Component("s3LambdaFunction")
public class S3LambdaFunction implements Function<S3EventNotification,S3EventNotification> {

    @Autowired
    private AmazonS3 amazonS3;

    @Override
    public S3EventNotification apply(S3EventNotification s3Event) {
        log.info("Function started with record {}", s3Event.toJson());

        for(S3EventNotification.S3EventNotificationRecord record : s3Event.getRecords()){
            final String key = record.getS3().getObject().getKey();
            final String bucketName = record.getS3().getBucket().getName();

            log.info("Received record from bucket {} and key {}", bucketName, key);

            S3Object object = amazonS3.getObject(new GetObjectRequest(bucketName, key));

            log.info("Object retrieved from bucket {}", object);
        }
        return s3Event;
    }
}
