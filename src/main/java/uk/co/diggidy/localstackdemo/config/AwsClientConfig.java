package uk.co.diggidy.localstackdemo.config;


import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class AwsClientConfig {

    @Value("${local.stack.endpoint}")
    private String localStackEndpoint;

    @Value("${local.stack.region}")
    private String region;

    @Bean
    @Profile("aws")
    public AmazonS3 awsS3Client(){
        return AmazonS3ClientBuilder.standard().build();
    }

    @Bean
    @Profile("local")
    public AmazonS3 awsS3ClientLocalStack(){
        return AmazonS3ClientBuilder.standard()
                .withEndpointConfiguration(
                        new AwsClientBuilder.EndpointConfiguration(
                                localStackEndpoint, region))
                .build();
    }
}
