variable "STAGE" {
  type    = string
  default = "local"
}

variable "AWS_REGION" {
  type    = string
  default = "eu-west-1"
}

variable "JAR_PATH" {
  type    = string
  default = "build/libs/localstack-demo-0.0.1-SNAPSHOT-aws.jar"
}

provider "aws" {
  access_key                  = ""
  secret_key                  = ""
  region                      = var.AWS_REGION
  s3_force_path_style         = true
  skip_credentials_validation = true
  skip_metadata_api_check     = true
  skip_requesting_account_id  = true

  endpoints {
    apigateway       = var.STAGE == "local" ? "http://localhost:4566" : null
    cloudformation   = var.STAGE == "local" ? "http://localhost:4566" : null
    cloudwatch       = var.STAGE == "local" ? "http://localhost:4566" : null
    cloudwatchevents = var.STAGE == "local" ? "http://localhost:4566" : null
    iam              = var.STAGE == "local" ? "http://localhost:4566" : null
    lambda           = var.STAGE == "local" ? "http://localhost:4566" : null
    s3               = var.STAGE == "local" ? "http://localhost:4566" : null
  }
}

resource "aws_iam_role" "lambda-execution-role1" {
  name = "lambda-execution-role1"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.s3LambdaFunction.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.bucket.arn
}

resource "aws_lambda_function" "s3LambdaFunction" {
  filename         = var.JAR_PATH
  function_name    = "s3LambdaFunction"
  role             = aws_iam_role.lambda-execution-role1.arn
  handler          = "org.springframework.cloud.function.adapter.aws.FunctionInvoker::handleRequest"
  runtime          = "java11"
  memory_size      = 512
  timeout          = 30
  source_code_hash = filebase64sha256(var.JAR_PATH)
  environment {
    variables = {
      SPRING_PROFILES_ACTIVE = "aws"
    }
  }
}


resource "aws_s3_bucket" "bucket" {
  bucket = "input-bucket-dale223322"
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.s3LambdaFunction.arn
    events              = ["s3:ObjectCreated:*"]
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}